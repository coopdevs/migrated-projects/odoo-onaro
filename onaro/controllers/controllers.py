from odoo.addons.base_rest.controllers import main



class MainController(main.RestController):
    _root_path = "/api/"
    _collection_name = "onaro.services"
    _default_auth = "api_key"


class CrmLeadController(MainController):
    pass


class HelpdeskController(MainController):
    pass
